# python_package_zarr_in_swift

Create a python package for an easy data conversion from netCDF to zarr and upload into the swift cloud.

Following this tutorial: [Packaging Python Projects](https://packaging.python.org/tutorials/packaging-projects/)

More info: [Description Draft](https://docs.google.com/document/d/1rz782sUO-bpjw9GYhDBybum9B6Fy_PfKjEA4TblUgec/edit?usp=sharing)
